# party-build-web

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

# 项目介绍

## 组件分类

### `index` 首页模块
### `news` 新闻模块
### `seminar` 专题模块
### `studyCenter` 学习中心
### `partyMien` 党员风采
### `meetingCenter` 会议中心
### `dataOverview` 数据总览

