import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
		{
			path: '/login',
			name:'/login',
			component: resolve => require(['@/components/login'], resolve),
		},
    {
      path: '/',
      component: resolve => require(['@/components/Index'], resolve),
			redirect: '/login',
      children: [
        {
          path: '/index',
          component: resolve => require(['@/components/index/Index'], resolve)
        },
				{
				  path: '/index-list/:id',
				  component: resolve => require(['@/components/index/list'], resolve)
				},
				{
				  path: '/index-vanguard/:id',
				  component: resolve => require(['@/components/index/vanguard'], resolve)
				},
				{
				  path: '/news',
				  component: resolve => require(['@/components/news/Index'], resolve)
				},
				{
				  path: '/news-detail/:id/:type',
				  component: resolve => require(['@/components/news/detail'], resolve)
				},
				{
				  path: '/seminar',
				  component: resolve => require(['@/components/seminar/Index'], resolve)
				},
				{
				  path: '/seminar-detail/:id',
				  component: resolve => require(['@/components/seminar/detail'], resolve)
				},
				{
				  path: '/seminar-content/:type/:id',
				  component: resolve => require(['@/components/seminar/content'], resolve)
				},
				{
				  path: '/studyCenter',
					component: resolve => require(['@/components/studyCenter/Index'], resolve)
				},
				{
					path: '/studyCente-detail',
					redirect: '/studyCente-detail/courseWare',
					component: resolve => require(['@/components/studyCenter/Detail'], resolve),
					children: [
						{
							path: 'courseWare',
							component: resolve => require(['@/components/studyCenter/detail/CourseWare'], resolve)
						},
						{
							path: 'notes',
							component: resolve => require(['@/components/studyCenter/detail/Notes'], resolve)
						},
						{
							path: 'courseWareDetail/:index',
							component: resolve => require(['@/components/studyCenter/detail/CourseWareDetail'], resolve)
						}
					]
				},
        {
          path: '/dataOverview',
          component: resolve => require(['@/components/dataOverview/Index'], resolve)
        },
				{
				  path: '/partyMien',
				  component: resolve => require(['@/components/partyMien/Index'], resolve)
				},
				{
				  path: '/partyMien-detail/:id',
				  component: resolve => require(['@/components/partyMien/detail'], resolve)
				},
				{
				  path: '/personalCenter',
				  component: resolve => require(['@/components/personalCenter/Index'], resolve)
				},
				{
				  path: '/meetingCenter',
				  component: resolve => require(['@/components/meetingCenter/Index'], resolve)
				},
				{
				  path: '/meetingCenter-detail',
				  component: resolve => require(['@/components/meetingCenter/detail'], resolve)
				}
      ]
    }
  ]
})
