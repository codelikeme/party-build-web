// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Axios from './axios.config'  //引入全局组件
import global_ from './components/Global'  //修改原型

import VideoPlayer from 'vue-video-player'
import '../node_modules/video.js/dist/lang/zh-CN'
import '../node_modules/video.js/dist/video-js.css'
import '../node_modules/vue-video-player/src/custom-theme.css'

Vue.prototype.GLOBAL = global_

Vue.use(ElementUI)
Vue.use(VideoPlayer)
Vue.prototype.$axios = Axios

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})